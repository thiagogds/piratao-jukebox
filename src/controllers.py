import pygame
from pygame.locals import *

from models import VideoQueue
from events import *

class CreditsVaultController(object):
    def __init__(self, evManager, vault):
        self.evManager = evManager
        self.evManager.RegisterListener(self)
        self.vault = vault


    def notify(self, event):
        if isinstance(event, TickEvent):
            for event in event.py_events:
                if event.type == KEYDOWN and event.key == K_w:
                    self.vault.add_credits()
                    ev = ChangeCreditsEvent(True)
                    self.evManager.post(ev)

        if isinstance(event, AddVideoEvent):
            self.vault.remove_credits()
            ev = ChangeCreditsEvent(False)
            self.evManager.post(ev)


class VideoManageController(object):

    def __init__(self, evManager, vault, video_queue):
        self.evManager = evManager
        self.evManager.RegisterListener(self)

        self.video_queue = video_queue
        self.vault = vault

        self.playing = False

    def add_video(self, path):
        self.video_queue.add_video(path)
        ev = ChangeVideoQueueEvent()
        self.evManager.post(ev)

    def play_video(self):
        self.playing = True
        video_path = self.video_queue.get_video()
        self.evManager.post(PlayVideoEvent(video_path))


    def notify(self, event):
        if isinstance(event, AddVideoEvent):
            if self.vault.credits > 0:
                self.add_video(event.path)
        if isinstance(event, VideoStopEvent):
            self.playing = False

        if not self.playing and self.video_queue.video_list:
            self.play_video()


class DialogController(object):
    def __init__(self, evManager):
        self.evManager = evManager
        self.evManager.RegisterListener(self)

        self.playing = False
        self.video_resized = False

    def notify(self, event):
        if isinstance(event, TickEvent):
            for event in event.py_events:
                evs = []
                if event.type == KEYDOWN and event.key == K_LEFT:
                    if not self.video_resized:
                        evs.append(CloseDialogEvent())
                        self.evManager.UnregisterListener(self)
                        new_ctrl = ArtistsIndexController(self.evManager)
                if event.type == KEYDOWN and event.key == K_SPACE:
                    evs.append(SelectVideoEvent())
                if event.type == KEYDOWN and event.key == K_UP:
                    evs.append(UpEvent())
                if event.type == KEYDOWN and event.key == K_DOWN:
                    evs.append(DownEvent())

                if evs:
                    for ev in evs:
                        self.evManager.post(ev)
                    break;

        if isinstance(event, VideoResizedEvent):
            self.video_resized = event.resized
            if self.video_resized:
                self.evManager.post(CloseDialogEvent())
                self.evManager.UnregisterListener(self)
                new_ctrl = ArtistsIndexController(self.evManager)
                self.evManager.post(VideoResizedEvent(self.video_resized))


class ArtistsIndexController(object):
    def __init__(self, evManager):
        self.evManager = evManager
        self.evManager.RegisterListener(self)

        self.video_resized = False
    def notify(self, event):
        if isinstance(event, TickEvent):
            for event in event.py_events:
                ev = None
                if event.type == KEYDOWN and self.video_resized and not event.key == K_a:
                        ev = ShrinkVideoEvent()
                        self.evManager.post(VideoResizedEvent(False))
                else:
                    if event.type == KEYDOWN and event.key == K_RIGHT:
                        ev = NextEvent()
                    if event.type == KEYDOWN and event.key == K_LEFT:
                        ev = PreviousEvent()
                    if event.type == KEYDOWN and event.key == K_UP:
                        ev = PreviousLetterEvent()
                    if event.type == KEYDOWN and event.key == K_DOWN:
                        ev = NextLetterEvent()
                    if event.type == KEYDOWN and event.key == K_SPACE:
                        ev = SelectEvent()
                        self.evManager.UnregisterListener(self)
                        new_ctrl = DialogController(self.evManager)
                if ev:
                    self.evManager.post(ev)
                    break;

        if isinstance(event, VideoResizedEvent):
            self.video_resized = event.resized
