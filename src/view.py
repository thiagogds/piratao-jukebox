# -*- coding: utf-8 -*-
import math

from unipath import Path, DIRS

import pygame
from pygame.locals import *

try:
    from cStringIO import StringIO as BytesIO
except ImportError:
    from io import BytesIO

from models import Artist
from events import *

from gui import GUIOption, GUIButton, GUIDisplay, GUIInfoDisplay
from settings import ASSETS_PATH, VIDEOS_PATH, IMG_MODE


class ArtistsIndexView(object):
    def __init__(self, master_view):
        self.evManager = master_view.evManager
        self.evManager.RegisterListener(self)
        self.master_view = master_view

        self.dialog = False
        self.resized = False
        self.was_full_screen = True

        self.preview = False
        self.video = None

        self.FULLSCREEN_TIME = 100
        self.video_time = 0
        self.margin_x = 50
        self.margin_y = 60
        self.background_margin_y = 60

        self.screen_size = self.screen_width, self.screen_height = master_view.window.get_size()

        self.background = pygame.Surface(self.screen_size)
        self.background_image = pygame.image.load(ASSETS_PATH.child("fundo.jpg")).convert()
        self.background_image = self.background_image.convert(self.background_image.get_masks(), IMG_MODE)
        self.background_image = pygame.transform.scale(
            self.background_image,
            (
                self.screen_width,
                self.screen_height
            )
        )

        self.video_surface = pygame.Surface((520, 292))
        self.full_screen_surface = pygame.Surface(self.screen_size)
        self.color = [0, 0, 0]
        self.artist_selected = 0

        self.albums = VIDEOS_PATH.listdir(filter=DIRS)
        self.current_album = 0

        self.artists = []

        self.change_artist()

        self.credits_display = GUIDisplay(u'Créditos', ASSETS_PATH.child('caixa_creditos.png'))
        self.credits_surface = self.credits_display.draw(str(self.master_view.vault.credits))

        self.queue_display = GUIDisplay(u'Em Espera', ASSETS_PATH.child('caixa_espera.png'))
        self.queue_surface = self.queue_display.draw(
            str(len(self.master_view.video_queue.video_list))
        )
        self.info_display = GUIInfoDisplay()

        self.coin_anim_list = []

        forward = range(0, 10)
        backward = range(0, 10)
        backward.reverse()
        for i in forward + backward:
            coin_image = pygame.image.load(ASSETS_PATH.child('insert-coin-%d.png' % i))
            coin_image = coin_image.convert(coin_image.get_masks(), IMG_MODE)
            self.coin_anim_list.append(coin_image)

        self.anim_frame = 0


        self.video_dialog = VideosListDialog(self.artists[self.artist_selected])
        self.default_video_position = (640, 133)
        self.shifted_video_position = (self.margin_x, 133)

        self.offset_y = 5
        self.offset_x = 5
        self.cols = 6
        self.rows = int(math.ceil(len(self.artists) / float(self.cols)))
        self.max_rows = 3

        self.background.fill(self.color)
        self.background.blit(self.background_image, (self.margin_x, self.margin_y))

    def change_artist(self):
        self.artists = []
        self.buttons = []

        artists_path_list = self.albums[self.current_album].listdir()

        for path in artists_path_list:
            artist = Artist(
                    path.name.decode('utf-8'),
                    path.child('image.jpg'),
                    path.listdir(pattern="*.mpg")
                )
            self.artists.append(artist)
            self.buttons.append(artist.button.draw())

        self.artists[self.artist_selected].button.selected = True
        self.buttons[self.artist_selected] = self.artists[self.artist_selected].button.update_selected()

    def draw(self):
        if not self.resized:
            self.background.fill(self.color)
            self.background.blit(self.background_image, (0, 0))

            height = 0
            width = 0

            for row in range(self.rows):
                for col in range(self.cols):
                    if self.artist_index(row, col) >= len(self.artists):
                        break

                    button = self.buttons[self.artist_index(row, col)]

                    self.background.blit(
                        button,
                        (
                            (width+self.offset_x)*col + self.margin_x + 18,
                            (height+self.offset_y)*row + self.margin_y + 18
                        )
                    )
                    height = button.get_height()
                    width = button.get_width()

        if self.dialog and not self.resized:
            self.background.blit(self.video_dialog.draw(), (self.margin_x, self.background_margin_y))

        self.background.blit(
            self.credits_surface,
            (self.margin_x, self.background.get_height() - self.credits_surface.get_height())
        )

        self.background.blit(
            self.queue_surface,
            (
                self.margin_x + self.credits_surface.get_width() - 40,
                self.background.get_height() - self.queue_surface.get_height()
            )
        )

        coin_frame = self.coin_anim_list[self.anim_frame]
        self.background.blit(
            coin_frame, (
                self.screen_width - coin_frame.get_width() - self.margin_x,
                self.background.get_height() - coin_frame.get_height() - self.margin_y
            )
        )
        self.anim_frame = (self.anim_frame + 1) % len(self.coin_anim_list)

        if self.video and not self.preview:
            if int(self.video.get_time()) > self.old_time:
                self.info_surface = self.info_display.draw(
                    self.video_length(self.video.get_length()),
                    self.video_length(self.video.get_time()),
                    self.current_artist_name, self.current_video_title
                )
                self.old_time = int(self.video.get_time())
            self.background.blit(
                self.info_surface,
                (
                    self.margin_x + self.credits_surface.get_width() + self.queue_surface.get_width() - 80,
                    self.background.get_height() - self.info_surface.get_height() - 5
                )
            )


        if self.video:
            self.video_time += 1
            if self.resized:
                self.background.blit(self.full_screen_surface, (0, 0))
            else:
                col_1 = ((self.artist_selected + 1) % self.cols) == 0
                col_2 = ((self.artist_selected + 2) % self.cols) == 0
                col_3 = ((self.artist_selected + 3) % self.cols) == 0
                col_4 = ((self.artist_selected + 4) % self.cols) == 0
                if any((col_1, col_2, col_3)) and not self.dialog:
                    self.background.blit(
                        self.video_surface, self.shifted_video_position
                    )
                else:
                    self.background.blit(
                        self.video_surface, self.default_video_position
                    )

        if self.video and self.video_time == self.FULLSCREEN_TIME and not self.preview:
            self.enlarge_video()
            if self.resized:
                self.video_time == 101

        self.master_view.window.blit(self.background, (0, 0))

        pygame.display.update()

    def clear_video(self):
        self.video.stop()
        self.video = None
        if not self.master_view.video_queue.video_list:
            self.resized = False
        self.evManager.post(VideoStopEvent())

    def video_length(self, length):
        hours = 00
        minutes = 00
        seconds = 00

        if length > 3600:
            hours = int(length / 3600)
        length = length % 3600

        if length > 60:
            minutes = int(length / 60)

        seconds = int(length % 60)

        return '%s:%s:%s' % (str(hours).zfill(2), str(minutes).zfill(2), str(seconds).zfill(2))

    def shrink_video(self):
        if self.video:
            self.video.pause()
            (video_width, video_height) = self.video.get_size()
            (surface_width, surface_height) = self.video_surface.get_size()
            video_aspect_ratio = (surface_height * 1.0) / video_height
            video_width = video_width * video_aspect_ratio

            xpos = (surface_width - video_width) / 2

            self.video.set_display(self.video_surface, pygame.Rect(
                (xpos, 0),
                (video_width, surface_height)
            ))
            self.video.play()
        self.resized = False
        self.evManager.post(VideoResizedEvent(self.resized))

    def enlarge_video(self):
        self.video.pause()
        (video_width, video_height) = self.video.get_size()
        (surface_width, surface_height) = self.full_screen_surface.get_size()
        video_aspect_ratio = (surface_height * 1.0) / video_height
        video_width = video_width * video_aspect_ratio

        xpos = (surface_width - video_width) / 2

        self.video.set_display(self.full_screen_surface, pygame.Rect(
            (xpos, 0),
            (video_width, surface_height)
        ))
        self.video.play()
        self.resized = True
        self.evManager.post(VideoResizedEvent(self.resized))

    def artist_index(self, row, col):
        return row * self.cols + col

    def preview_video(self):
        artist_selected = self.artists[self.artist_selected]
        video_selected = artist_selected.videos[self.video_dialog.selected]

        self.preview = True
        self.play_video(video_selected)

    def next_artist(self):
        artist = self.artists[self.artist_selected].button
        artist.selected = False
        artist.update_selected()
        self.artist_selected += 1
        if self.artist_selected > len(self.artists) - 1:
            self.current_album += 1
            if self.current_album > len(self.albums) - 1:
                self.current_album = 0
            self.artist_selected = 0
            self.margin_y = 60
            self.change_artist()

        scroll_offset = self.artist_selected / self.cols
        if scroll_offset >= self.max_rows:
            self.margin_y = ((scroll_offset - self.max_rows) + 1) * (-artist.background.get_height() + 3)

        artist = self.artists[self.artist_selected].button.selected = True
        self.buttons[self.artist_selected] = self.artists[self.artist_selected].button.update_selected()

    def previous_artist(self):
        artist = self.artists[self.artist_selected].button
        artist.selected = False
        artist.update_selected()
        self.artist_selected -= 1

        if self.artist_selected < 0:
            self.current_album -= 1
            if self.current_album < 0:
                self.current_album = len(self.albums) - 1
            self.artist_selected = len(self.albums[self.current_album].listdir(filter=DIRS)) - 1
            self.change_artist()


        scroll_offset = self.artist_selected / self.cols
        if scroll_offset >= self.max_rows:
            self.margin_y = (self.max_rows - scroll_offset - 1) * (artist.background.get_height() - 3)
        else:
            self.margin_y = 60

        artist = self.artists[self.artist_selected].button.selected = True
        self.buttons[self.artist_selected] = self.artists[self.artist_selected].button.update_selected()

    def next_letter(self):
        self.current_album += 1
        if self.current_album > len(self.albums) - 1:
            self.current_album = 0
        self.artist_selected = 0
        self.margin_y = 60
        self.change_artist()

    def previous_letter(self):
        self.current_album -= 1
        if self.current_album < 0:
            self.current_album = len(self.albums) - 1
        self.artist_selected = 0
        self.margin_y = 60
        self.change_artist()

    def next_video(self):
        self.video_dialog.selected += 1
        if self.video_dialog.selected > len(self.video_dialog.artist.videos) - 1:
            self.video_dialog.selected = 0

        if ((not self.preview) and (not self.video)) or ((self.preview and self.video)):
            self.preview_video()

    def previous_video(self):
        self.video_dialog.selected -= 1
        if self.video_dialog.selected < 0:
            self.video_dialog.selected = len(self.video_dialog.artist.videos) - 1

        if ((not self.preview) and (not self.video)) or ((self.preview and self.video)):
            self.preview_video()

    def show_dialog(self):
        self.video_dialog = VideosListDialog(self.artists[self.artist_selected])
        self.dialog = True

        if ((not self.preview) and (not self.video)) or ((self.preview and self.video)):
            self.preview_video()

    def close_dialog(self):
        self.dialog = False
        if self.video and self.preview:
            self.clear_video()
            self.preview = False

    def play_video(self, path):
        self.old_time = 0
        if self.video:
            self.video.stop()
        self.video = None
        self.video_time = 0
        while pygame.mixer.get_busy():
            continue

        pygame.mixer.quit()

        self.video = pygame.movie.Movie(path)

        self.current_artist_name = path.parent.name
        self.current_video_title = path.name[:-4]

        (video_width, video_height) = self.video.get_size()
        (surface_width, surface_height) = self.video_surface.get_size()
        video_aspect_ratio = (surface_height * 1.0) / video_height

        video_width = video_width * video_aspect_ratio

        xpos = (surface_width - video_width) / 2
        self.video_surface.fill(self.color)
        self.full_screen_surface.fill(self.color)

        if self.resized and not self.preview:
            self.enlarge_video()
        else:
            self.video.set_display(self.video_surface, pygame.Rect(
                (xpos, 0),
                (video_width, surface_height)
            ))


        self.video.play()
        self.info_surface = self.info_display.draw(
            self.video_length(self.video.get_length()),
            self.video_length(self.video.get_time()),
            self.current_artist_name, self.current_video_title
        )

    def play_sound(self, sound_name):
        play = False
        if self.video and self.preview:
            self.clear_video()
            play = True
        elif not self.video:
            play = True

        if play:
            pygame.mixer.init()
            pygame.mixer.Sound(ASSETS_PATH.child(sound_name)).play()

    def notify(self, event):
        if isinstance(event, TickEvent):
            self.draw()
        else:
            self.video_time = 0
        if isinstance(event, NextEvent):
            self.next_artist()
        if isinstance(event, PreviousEvent):
            self.previous_artist()
        if isinstance(event, NextLetterEvent):
            self.next_letter()
        if isinstance(event, PreviousLetterEvent):
            self.previous_letter()
        if isinstance(event, CloseDialogEvent):
            self.close_dialog()
        if isinstance(event, ShrinkVideoEvent):
            self.shrink_video()
        if isinstance(event, SelectEvent):
            self.show_dialog()
        if isinstance(event, DownEvent):
            self.next_video()
        if isinstance(event, UpEvent):
            self.previous_video()
        if isinstance(event, PlayVideoEvent):
            self.play_video(event.path)
            self.queue_surface = self.queue_display.draw(
                str(len(self.master_view.video_queue.video_list))
            )
        if isinstance(event, ChangeVideoQueueEvent):
            self.queue_surface = self.queue_display.draw(
                str(len(self.master_view.video_queue.video_list))
            )
        if isinstance(event, SelectVideoEvent):
            self.play_sound('video_select.wav')
            self.preview = False
            artist_selected = self.artists[self.artist_selected]
            video_selected = artist_selected.videos[self.video_dialog.selected]
            self.evManager.post(AddVideoEvent(video_selected))

        if isinstance(event, ChangeCreditsEvent):
            self.credits_surface = self.credits_display.draw(str(self.master_view.vault.credits))
            if event.increase:
                self.play_sound('moeda.wav')

        if isinstance(event, ChangeVideoQueueEvent):
            self.credits_surface = self.credits_display.draw(str(self.master_view.vault.credits))
        if self.video and not self.video.get_busy() or isinstance(event, ForceStopEvent):
            self.clear_video()
        if self.video and self.video.get_time() > 10 and self.preview:
            self.clear_video()
            self.preview = False


class VideosListDialog(object):
    def __init__(self, artist):
        self.artist = artist
        percentage = 0.4
        self.selected = 0
        self.text_select = GUIOption(True)
        self.text = GUIOption(False)

        self.background = pygame.image.load(ASSETS_PATH.child('caixa_playlist.png'))
        self.background = self.background.convert_alpha()

    def draw(self):
        start_slice = 0
        end_slice = 18
        selected = self.selected

        if self.selected >= 18:
            start_slice = self.selected - 17
            end_slice = self.selected + 1
            selected = self.selected - (self.selected - 17)

        for i, path in enumerate(self.artist.videos[start_slice:end_slice]):
            name = path.name[:-4]
            if selected == i:
                text = self.text_select
            else:
                text = self.text

            text = text.draw(name.decode('utf-8'))
            self.background.blit(
                text, (
                    (self.background.get_width() - text.get_width()) / 2,
                    (text.get_height() * i) + 40
                )
            )

        return self.background
