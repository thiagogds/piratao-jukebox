# -*- coding: utf-8 -*-
import pygame
from settings import ASSETS_PATH, VIDEOS_PATH, IMG_MODE

class GUIButton(object):
    def __init__(self, image_path, selected=False):
        self.image_path = image_path
        self.selected = selected

        self.image = pygame.image.load(self.image_path).convert_alpha()
        self.background = pygame.Surface((182, 182), flags=IMG_MODE | pygame.SRCALPHA)

        self.frame = pygame.image.load(ASSETS_PATH.child('caixa_etiquetas.png')).convert_alpha()
        self.sel_frame = pygame.image.load(ASSETS_PATH.child('selecao_caixa.png')).convert_alpha()

    def draw(self):
        if self.selected:
            frame = self.sel_frame
        else:
            frame = self.frame

        self.background.blit(self.image, (10, 10))
        self.background.blit(frame, (0, 0))

        return self.background

    def update_selected(self):
        if self.selected:
            frame = self.sel_frame
        else:
            frame = self.frame

        self.background.blit(frame, (0, 0))

        return self.background


class GUIOption(object):
    def __init__(self, selected=False):
        self.selected = selected
        self.font = pygame.font.Font(None, 25)
        if selected:
            self.color = [255, 0, 0]
        else:
            self.color = [0, 0, 255]


    def draw(self, text):
        try:
            text = (text[:27] + '...') if len(text) > 27 else text
            return self.font.render(text, 1, self.color)
        except pygame.error:
            print "ERRO no GUIOption"
            pygame.quit()

class GUIDisplay(object):
    def __init__(self, up_text, image_path):
        self.up_font = pygame.font.Font(None, 35)
        self.bottom_font = pygame.font.Font(None, 60)
        self.up_text = up_text

        self.color = [255, 255, 0]

        self.display = pygame.image.load(image_path)
        self.display = self.display.convert(self.display.get_masks(), IMG_MODE)


    def draw(self, bottom_text):
        display = self.display.copy()

        try:
            up_font = self.up_font.render(self.up_text, 1, self.color)
            bottom_font = self.bottom_font.render(bottom_text, 1, self.color)
        except pygame.error:
            print "ERRO no GuiDisplay"
            pygame.quit()
        display.blit(
            up_font,
            ((self.display.get_width() - up_font.get_width()) / 2 - 6, 30)
        )
        display.blit(
            bottom_font,
            ((self.display.get_width() - bottom_font.get_width()) / 2 - 6, 58)
        )
        return display

class GUIInfoDisplay(object):
    def __init__(self):
        self.font = pygame.font.Font(None, 26)

        self.color = [0, 0, 255]

        self.display = pygame.image.load(ASSETS_PATH.child('caixa_info.png'))
        self.display = self.display.convert(self.display.get_masks(), IMG_MODE)

        self.clock = pygame.time.Clock()

    def draw(self, total_time, video_time, artist_name, video_title):
        display = self.display.copy()
        info = artist_name.decode('utf-8') + ' - ' + video_title.decode('utf-8')
        video_info_str = (info[:38] + '...') if len(info) > 38 else info

        try:
            video_info = self.font.render(video_info_str, 1, self.color)
            video_time = self.font.render('%s/%s' % (video_time, total_time), 1, self.color)
        except pygame.error:
           print "ERRO no GuiInfoDisplay"
           pygame.quit()

        display.blit(video_info, (40, 30))
        display.blit(video_time, (40, 60))
        return display
