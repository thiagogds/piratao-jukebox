# -*- coding: utf-8 -*-

import pygame
from pygame.locals import *

from coopy.base import init_persistent_system
from unipath import Path, DIRS

from models import VideoQueue, CreditsVault
from events import TickEvent, QuitEvent, ChangeScreenEvent, ForceStopEvent
from controllers import ArtistsIndexController, VideoManageController, CreditsVaultController
from view import ArtistsIndexView

class CPUSpinnerController:
    """..."""
    def __init__(self, evManager):
        self.evManager = evManager
        self.evManager.RegisterListener(self)
        self.clock = pygame.time.Clock()

        self.keepGoing = 1

    def Run(self):
        while self.keepGoing:
            self.clock.tick(30)
            event = TickEvent(py_events=pygame.event.get())
            self.evManager.post(event)

    def notify(self, event):
        if isinstance(event, QuitEvent):
            # This will stop the while loop from running.
            self.keepGoing = 0

class EventManager(object):
    """
    This object is responsible for coordinating most communication between the
    Model, View, and Controller.
    """
    def __init__(self):
        self.listeners = {}

    def RegisterListener(self, listener):
        self.listeners[listener] = 1

    def UnregisterListener(self, listener):
        if listener in self.listeners.keys():
            del self.listeners[listener]

    def post(self, event):
        for listener in self.listeners.keys():
            listener.notify(event)

class PygameMasterController(object):
    def __init__(self, evManager, vault, video_queue):
        self.evManager = evManager
        self.evManager.RegisterListener(self)

        video_ctr = VideoManageController(evManager, vault, video_queue)
        credits_ctr = CreditsVaultController(evManager, vault)
        self.evManager.post(ChangeScreenEvent(ctr_class=ArtistsIndexController))

    def switch_controller(self, ctr_class):
        if ctr_class:
            new_controller = ctr_class(self.evManager)

    def notify(self, event):
        if isinstance(event, TickEvent):
            for event in event.py_events:
                ev = None
                if event.type == KEYDOWN and event.key == K_ESCAPE:
                    ev = QuitEvent()
                if event.type == KEYDOWN and event.key == K_a:
                    ev = ForceStopEvent()
                if ev:
                    self.evManager.post(ev)

        if isinstance( event, ChangeScreenEvent ):
            self.switch_controller( event.ctr_class )

class PygameMasterView(object):
    def __init__(self, evManager, vault, video_queue):
        self.evManager = evManager
        self.evManager.RegisterListener(self)

        self.video_queue = video_queue
        self.vault = vault

        pygame.init()
        d_info = pygame.display.Info()
        size = width, height = (1280, 720)

        mode = pygame.FULLSCREEN | pygame.HWSURFACE | pygame.ANYFORMAT
        self.window = pygame.display.set_mode(size, mode)
        self.background = pygame.Surface(self.window.get_size())
        self.color = [0, 0, 0]

        self.background.fill(self.color)
        self.window.blit(self.background, (0, 0))
        pygame.display.update()

        self.evManager.post(ChangeScreenEvent(view_class=ArtistsIndexView))

    def switch_view(self, view_class):
        if view_class:
            view = view_class(self)

    def notify(self, event):
        if isinstance(event, ChangeScreenEvent):
            self.switch_view(event.view_class)

def main():
    evManager = EventManager()

    vault = init_persistent_system(CreditsVault)
    video_queue = VideoQueue()

    spinner = CPUSpinnerController(evManager)
    master_ctr = PygameMasterController(evManager, vault, video_queue)
    master_view = PygameMasterView(evManager, vault, video_queue)

    pygame.mouse.set_visible(False)

    spinner.Run()

if __name__ == '__main__':
    main()

