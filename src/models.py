import pygame
from gui import GUIButton

class Artist(object):
    def __init__(self, name, image_path, videos):
        self.name = name
        self.videos = videos
        self.image_path = image_path
        self.button = GUIButton(self.image_path)


class VideoQueue(object):
    def __init__(self):
        self.video_list = []

    def add_video(self, path):
        self.video_list.append(path)

    def get_video(self):
        return self.video_list.pop(0)

class CreditsVault(object):
    def __init__(self):
        self.credits = 0

    def add_credits(self, qty=2):
        self.credits += qty

    def remove_credits(self, qty=1):
        temp_credits = self.credits - qty
        if temp_credits >= 0:
            self.credits = temp_credits
