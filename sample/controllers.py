class KeyboardController:
    """..."""
    def __init__(self, evManager):
        self.evManager = evManager
        self.evManager.RegisterListener(self)

    def Notify(self, event):
        if isinstance(event, TickEvent):
            # Handle input events.
            for event in pygame.event.get():
                ev = None
                if event.type == QUIT:
                    ev = QuitEvent()
                elif event.type == KEYDOWN and event.key == K_ESCAPE:
                    ev = QuitEvent()
                elif event.type == KEYDOWN and event.key == K_UP:
                    direction = DIRECTION_UP
                    ev = CharactorMoveRequest(direction)
                elif event.type == KEYDOWN and event.key == K_DOWN:
                    direction = DIRECTION_DOWN
                    ev = CharactorMoveRequest(direction)
                elif event.type == KEYDOWN and event.key == K_LEFT:
                    direction = DIRECTION_LEFT
                    ev = CharactorMoveRequest(direction)
                elif event.type == KEYDOWN and event.key == K_RIGHT:
                    direction = DIRECTION_RIGHT
                    ev = CharactorMoveRequest(direction)

                if ev:
                    self.evManager.Post(ev)


class CPUSpinnerController:
    """..."""
    def __init__(self, evManager):
        self.evManager = evManager
        self.evManager.RegisterListener(self)

        self.keepGoing = 1

    def Run(self):
        while self.keepGoing:
            event = TickEvent()
            self.evManager.Post(event)

    def Notify(self, event):
        if isinstance(event, QuitEvent):
            # This will stop the while loop from running.
            self.keepGoing = 0
