def main():
    """..."""
    evManager = EventManager()

    keybd = KeyboardController(evManager)
    spinner = CPUSpinnerController(evManager)
    pygameView = PygameView(evManager)
    game = Game(evManager)

    spinner.Run()

if __name__ == '__main__':
    main()

