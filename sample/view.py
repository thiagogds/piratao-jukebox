import pygame
from pygame.locals import *

class SectorSprite(pygame.sprite.Sprite):
    def __init__(self, sector, group=None):
        pygame.sprite.Sprite.__init__(self, group)
        self.image = pygame.Surface((132, 132))
        self.image.fill((0, 255, 128))

        self.sector = sector

class CharactorSprite(pygame.sprite.Sprite):
    def __init__(self, group=None):
        pygame.sprite.Sprite.__init__(self, group)

        charactorSurf = pygame.Surface((64, 64))
        #import inspect
        #print inspect.getargspec(pygame.draw.rect)
        pygame.draw.rect(charactorSurf, (0, 255, 128), ((0,0), (64,64)))
        pygame.draw.circle(charactorSurf, (255, 0, 0), (32, 32), 32)
        self.image = charactorSurf
        self.rect  = charactorSurf.get_rect()

        self.moveTo = None

    def update(self):
        if self.moveTo:
            self.rect.center = self.moveTo
            self.moveTo = None

class PygameView:
    """..."""
    def __init__(self, evManager):
        self.evManager = evManager
        self.evManager.RegisterListener(self)

        pygame.init()
        self.window = pygame.display.set_mode((428, 428))
        pygame.display.set_caption('Example Game')
        self.background = pygame.Surface(self.window.get_size())
        self.background.fill((0, 0, 0))

        self.backSprites = pygame.sprite.RenderUpdates()
        self.frontSprites = pygame.sprite.RenderUpdates()


    def ShowMap(self, map):
        squareRect = pygame.Rect((-128, 10, 128, 128))

        i = 0
        for sector in map.sectors:
            if i < 3:
                squareRect = squareRect.move(138, 0)
            else:
                i = 0
                squareRect = squareRect.move(-(138 * 2), 138)
            i += 1
            newSprite = SectorSprite(sector, self.backSprites)
            newSprite.rect = squareRect
            newSprite = None

    def ShowCharactor(self, charactor):
        charactorSprite = CharactorSprite(self.frontSprites)

        sector = charactor.sector
        sectorSprite = self.GetSectorSprite(sector)
        charactorSprite.rect.center = sectorSprite.rect.center

    def MoveCharactor(self, charactor):
        charactorSprite = self.GetCharactorSprite(charactor)

        sector = charactor.sector
        sectorSprite = self.GetSectorSprite(sector)

        charactorSprite.moveTo = sectorSprite.rect.center

    def GetCharactorSprite(self, charactor):
        #there will be only one
        for s in self.frontSprites.sprites():
            return s

    def GetSectorSprite(self, sector):
        for s in self.backSprites.sprites():
            if hasattr(s, 'sector') and s.sector == sector:
                return s


    def Notify(self, event):
        if isinstance(event, TickEvent):
            #Draw Everything
            self.backSprites.clear( self.window, self.background)
            self.frontSprites.clear( self.window, self.background)

            self.backSprites.update()
            self.frontSprites.update()

            dirtyRects1 = self.backSprites.draw(self.window)
            dirtyRects2 = self.frontSprites.draw(self.window)

            dirtyRects = dirtyRects1 + dirtyRects2
            pygame.display.update(dirtyRects)


        elif isinstance(event, MapBuiltEvent):
            map = event.map
            self.ShowMap(map)

        elif isinstance(event, CharactorPlaceEvent):
            self.ShowCharactor(event.charactor)

        elif isinstance(event, CharactorMoveEvent):
            self.MoveCharactor(event.charactor)


